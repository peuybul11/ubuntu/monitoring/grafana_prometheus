# Grafana_Prometheus

- **Password NodeExporter** = P@ssw0rd

- **Ubah target monitoring pada** = storage/prometheus/prometheus.yml

- **Data Source prometheus** = http://prometheus:9090

### Lalu jalankan docker dengan perintah
- sudo chmod 777 -R storage
- docker-compose up --build -d

## Node Exporter Dashboard ID Template, import grafana
- Node Exporter 1        : 1860
- Node Exporter 2        : 11074
- Node Exporter 3        : 405

## Grafana add clock plugin
```
docker exec -it grafana bash
```
```
grafana-cli plugins install grafana-clock-panel
```
```
exit
```
```
docker restart grafana
```